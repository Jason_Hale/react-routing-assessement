import React from 'react'

function NoMatch(props){
    return (
        <div className= "NoMatch">
            <h1>404 Page Not Found</h1>
        </div>
    );
}

export default NoMatch