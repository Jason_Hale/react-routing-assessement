import React from 'react';
import './App.css';
import Welcome from './components/welcome/Welcome';
import {Route, Switch} from 'react-router-dom';
import NoMatch from './components/nomatch/NoMatch';

function App() {
  return (
    <div className="App">
      <Switch>
      <Route exact path = '/' render = {(props) => <Welcome {...props}name="Everyone!"/>}
      />
   <Route exact path = '/welcome/:name' render = {(props) => <Welcome {...props}name= {props.match.params.name}/>}/>
   <Route path = '*' component = {NoMatch}/>
   {/* <Route path = '/clock' component = {Clock}/> */}
      </Switch>
    </div>
  );
}

export default App;
